board = makeEmptyPicture(400, 400, blue)
width = 50
startX = 0
startY = 0
for x in range(0,8):
  startX = x * 50
  for y in range(0,8):
    startY = y * 50
    sum = x + y
    if(sum % 2 == 1):
      addRectFilled(board,startX,startY,50,50,orange)
show(board)
writePictureTo(board, "/Users/kford2017/Documents/week3/board.jpg")

